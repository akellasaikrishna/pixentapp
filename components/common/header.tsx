import React, { Component, Fragment } from "react";
import {
  Platform,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Dimensions,
} from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import { StatusBar } from "expo-status-bar";

interface state {
  notificationCount: number;
  authToken: string;
}

const dimensions = Dimensions.get("screen");
export default class Header extends Component<any, state> {
  constructor(props: any) {
    super(props);
    this.state = {
      notificationCount: 0,
      authToken: "",
    };
  }
  componentDidMount() {}
  render() {
    return (
      <SafeAreaView>
        <StatusBar style="light" hidden={true} />
        <View
          style={{
            paddingTop: Platform.OS === "android" ? 20 : 0,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            style={{
              padding: 15,
              width: 55,
              backgroundColor: "#3F51B5",
              borderTopRightRadius: 55,
              borderBottomRightRadius: 55,
              elevation: 20,
            }}
            onPress={this.props.navigation.openDrawer}
          >
            <FontAwesome5 name="bars" size={20} color="#FFFFFF" />
          </TouchableOpacity>
          <View
            style={{
              position: "relative",
              width: Math.round(dimensions.width) - 110,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-around",
              }}
            >
              <Text style={this.props.style}>{this.props.title}</Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
