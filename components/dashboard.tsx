import React, { Component, Fragment } from "react";
import {
  Text,
  View,
  Image,
  Platform,
  Dimensions,
  BackHandler,
} from "react-native";
import Header from "./common/header";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";
import WebView from "react-native-webview";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import * as ScreenOrientation from "expo-screen-orientation";
const gamesFile = require("./helpers/games.json");

interface state {
  games: any;
  selectedGame: string;
  backButtonRegistered: boolean;
  screenOrientation: number;
}

const dimensions = Dimensions.get("window");
export default class Dashboard extends Component<any, state> {
  constructor(props: any) {
    super(props);
    this.state = {
      games: [],
      selectedGame: "",
      backButtonRegistered: false,
      screenOrientation: 1,
    };
  }
  backButtonHandler = () => {
    if (this.state.selectedGame.length > 0) {
      setTimeout(() => {
        this.setState({ selectedGame: "" });
      }, 1000);
    }
    return true;
  };
  orientationListener = (data: any) => {
    this.setState({
      screenOrientation: data["orientationInfo"]["orientation"],
    });
  };
  componentDidMount() {
    console.log(dimensions.height);
    console.log(dimensions.width);
    ScreenOrientation.removeOrientationChangeListener;
    ScreenOrientation.addOrientationChangeListener(this.orientationListener);
    BackHandler.addEventListener("hardwareBackPress", this.backButtonHandler);
    this.setState({
      games: gamesFile["games"],
    });
  }
  render() {
    return (
      <LinearGradient
        start={[0, 1]}
        end={[1, 0]}
        colors={["#000000", "#004586"]}
        style={{ flex: 1 }}
      >
        {this.state.selectedGame.length == 0 ? (
          <Fragment>
            {this.state.screenOrientation == 3 ||
            this.state.screenOrientation == 4 ? (
              <StatusBar style="light" hidden={true} />
            ) : (
              <View>
                <Header
                  title="LOBBY"
                  style={{
                    width: Math.round(dimensions.width / 1.4),
                    textAlign: "center",
                    color: "#FFFFFF",
                    fontSize: 22,
                    fontFamily: "Lexend",
                  }}
                  navigation={this.props.navigation}
                />
              </View>
            )}
            <ScrollView
              scrollEventThrottle={16}
              snapToAlignment="center"
              showsHorizontalScrollIndicator={false}
              horizontal={
                this.state.screenOrientation == 3 ||
                this.state.screenOrientation == 4
                  ? true
                  : false
              }
            >
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  marginHorizontal:
                    this.state.screenOrientation == 3 ||
                    this.state.screenOrientation == 4
                      ? 40
                      : 20,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    flexWrap: "wrap",
                    justifyContent: "space-around",
                  }}
                >
                  {this.state.games &&
                    this.state.games.map((item: any, key: any) => {
                      const image =
                        item["gameCode"] == "swanhouse"
                          ? require("../assets/swanhouse.png")
                          : item["gameCode"] == "aaposquest"
                          ? require("../assets/aaposquest.png")
                          : item["gameCode"] == "fabulousfairies"
                          ? require("../assets/fabulousfairies.png")
                          : item["gameCode"] == "9planetshockers"
                          ? require("../assets/9planetshockers.png")
                          : require("../assets/bookofamduat.png");
                      return (
                        <TouchableOpacity
                          style={{
                            marginVertical: 9,
                          }}
                          key={key}
                          onPress={() => {
                            this.setState({ selectedGame: item.link });
                          }}
                        >
                          <View
                            style={{
                              width:
                                this.state.screenOrientation == 3 ||
                                this.state.screenOrientation == 4
                                  ? dimensions.width / 2
                                  : 160,
                              height:
                                this.state.screenOrientation == 3 ||
                                this.state.screenOrientation == 4
                                  ? dimensions.width / 2
                                  : 160,
                              borderRadius: 20,
                              margin: 5,
                              backgroundColor: "#FFFFFF",
                              justifyContent: "center",
                              overflow: "hidden",
                            }}
                          >
                            <View>
                              <Image
                                source={image}
                                style={{
                                  width:
                                    this.state.screenOrientation == 3 ||
                                    this.state.screenOrientation == 4
                                      ? dimensions.width / 2
                                      : 160,
                                  height:
                                    this.state.screenOrientation == 3 ||
                                    this.state.screenOrientation == 4
                                      ? dimensions.width / 2
                                      : 160,
                                }}
                              />
                            </View>
                            <View
                              style={{
                                position: "absolute",
                                backgroundColor: "#FFFFFF",
                                borderTopRightRadius: 20,
                                bottom: -1,
                              }}
                            >
                              <View
                                style={{
                                  justifyContent: "center",
                                  alignItems: "center",
                                  width: 110,
                                  height: 55,
                                }}
                              >
                                <Text
                                  style={
                                    Platform.OS == "android"
                                      ? {
                                          color: "#616161",
                                          fontWeight: "bold",
                                          padding: 5,
                                          fontSize: 14,
                                          fontFamily: "Lexend",
                                          textAlign: "center",
                                          textAlignVertical: "center",
                                        }
                                      : {
                                          color: "#616161",
                                          fontWeight: "bold",
                                          padding: 5,
                                          fontSize: 14,
                                          fontFamily: "Lexend",
                                        }
                                  }
                                >
                                  {item.name}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                </View>
              </View>
            </ScrollView>
          </Fragment>
        ) : (
          <Fragment>
            <StatusBar style="light" hidden={true} />
            <WebView
              javaScriptEnabled={true}
              domStorageEnabled={true}
              startInLoadingState={true}
              style={{
                width: "100%",
              }}
              source={{
                uri: this.state.selectedGame,
              }}
            />
          </Fragment>
        )}
      </LinearGradient>
    );
  }
}
